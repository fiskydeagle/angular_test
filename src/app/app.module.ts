import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {ProjectModule} from "./project/project.module";
import {EmployeeModule} from "./employee/employee.module";

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        RouterModule.forRoot([
            { path: 'home', component: HomeComponent },
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: '**', redirectTo: '/', pathMatch: 'full' }
        ]),
        NgbModule.forRoot(),
        ProjectModule,
        EmployeeModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }

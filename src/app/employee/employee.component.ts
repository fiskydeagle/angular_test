import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {IEmployee} from "./employee";
import {EmployeeService} from "./employee.service";
import {DatePipe} from "@angular/common";
import {IProject} from "../project/project";

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

    employees: IEmployee[];
    employee: IEmployee = {
        id: -1,
        firstName: '',
        lastName: '',
        title: '',
        dateOfBirth: '',
        projets: [],
    };
    employeeProjects: IProject;
    employeeForm: FormGroup;
    dateOfBirthError: boolean = false;

    constructor(private _titleService: Title, private _employeeService: EmployeeService, private _datePipe: DatePipe) {
        this._titleService.setTitle( 'Angular Test - Employees' );
    }

    ngOnInit() {
        this._employeeService.getEmployees()
            .subscribe(employees => {
                this.employees = employees;
            });

        this.employeeForm = new FormGroup({
            id: new FormControl('', [
                Validators.required
            ]),
            firstName: new FormControl('', [
                Validators.minLength(3),
                Validators.required
            ]),
            lastName: new FormControl('', [
                Validators.minLength(3),
                Validators.required
            ]),
            title: new FormControl('', [
                Validators.minLength(3),
                Validators.required
            ]),
            dateOfBirth: new FormControl('', [
                Validators.required
            ])
        });
        this.employeeForm.patchValue(this.employee);
    }

    submitEmployeeForm(employee):void {
        let thisId = employee.id;
        if(thisId == -1){
            let nextId = Math.max.apply(Math,this.employees.map((employee: IEmployee) => employee.id))+1;

            employee.id = nextId;
            this.employees.push(employee);
        } else {
            let editEmployee = this.employees.filter((e: IEmployee) => {
                return e.id == thisId;
            });
            editEmployee[0].firstName = employee.firstName;
            editEmployee[0].lastName = employee.lastName;
            editEmployee[0].title = employee.title;
            editEmployee[0].dateOfBirth = employee.dateOfBirth;
            editEmployee[0].projets = [];
        }
        this.employeeForm.reset();
        this.employeeForm.patchValue(this.employee);
    }

    newEmployee():void {
        this.employeeForm.reset();
        this.employeeForm.patchValue(this.employee);
    }

    editEmployee(employee):void {
        let dateOfBirth = new Date(employee.dateOfBirth);
        let editEmployee = {
            id: employee.id,
            firstName: employee.firstName,
            lastName: employee.lastName,
            title: employee.title,
            dateOfBirth: this._datePipe.transform(dateOfBirth, 'yyyy-MM-dd')
        };

        this.employeeForm.patchValue(editEmployee);

        this.employeeProjects = employee.projets;
    }

    checkDateOfBirth(value):void {
        let d = new Date(value);

        if(d.getTime()) {
            this.dateOfBirthError = false;
        } else {
            this.dateOfBirthError = true;
        }
    }

}

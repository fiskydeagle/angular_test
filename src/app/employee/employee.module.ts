import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {RouterModule} from "@angular/router";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {EmployeeComponent} from "./employee.component";
import {EmployeeService} from "./employee.service";
import {KeyModule} from "../key/key.module";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            { path: 'employees', component: EmployeeComponent }
        ]),
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        KeyModule
    ],
    declarations: [
        EmployeeComponent
    ],
    providers: [
        EmployeeService,
        DatePipe
    ]
})
export class EmployeeModule { }

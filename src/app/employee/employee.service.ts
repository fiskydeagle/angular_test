import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {IEmployee} from "./employee";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class EmployeeService {

    constructor(private _http:HttpClient) {}

    //getEmployees(): Observable<IEmployee[]> {
    getEmployees(): Observable<any> {
        return this._http.get('/assets/api/employees.json', httpOptions);
    }
}

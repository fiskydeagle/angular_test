import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {KeyPipe} from "./key.pipe";

@NgModule({
    imports: [BrowserModule],
    declarations: [KeyPipe],
    exports: [KeyPipe]
})
export class KeyModule { }
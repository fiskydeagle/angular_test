import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'key',
  pure: false
})
export class KeyPipe implements PipeTransform {

    transform(value: any, args?: any[]): any[] {

        let keyArr: any[] = Object.keys(value[0]),
            dataArr = [];

        keyArr.forEach((key: any) => {
            dataArr.push({
                key: key,
                formatted: key.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase(); })
            });
        });

        return dataArr;
    }

}

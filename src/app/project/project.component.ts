import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ProjectService} from "./project.service";
import {IProject} from "./project";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

    projects: IProject[];
    project: IProject = {
        id: -1,
        name: '',
        startDate: '',
        endDate: '',
        status: 'ACTIVE',
    };
    projectForm: FormGroup;
    startDateError: boolean = false;
    endDateError: boolean = false;

    constructor(private _titleService: Title, private _projectService: ProjectService, private _datePipe: DatePipe) {
        this._titleService.setTitle( 'Angular Test - Projects' );
    }

    ngOnInit() {
        this._projectService.getProjects()
            .subscribe(projects => {
                this.projects = projects;
            });

        this.projectForm = new FormGroup({
            id: new FormControl('', [
                Validators.required
            ]),
            name: new FormControl('', [
                Validators.minLength(3),
                Validators.required
            ]),
            startDate: new FormControl('', [
                Validators.required
            ]),
            endDate: new FormControl('', [
                Validators.required
            ]),
            status: new FormControl('', [
                Validators.required
            ])
        });
        this.projectForm.patchValue(this.project);
    }

    submitProjectForm(project):void {
        let thisId = project.id;
        if(thisId == -1){
            let nextId = Math.max.apply(Math,this.projects.map((project: IProject) => project.id))+1;

            project.id = nextId;
            this.projects.push(project);
        } else {
            let editProject = this.projects.filter((p: IProject) => {
                return p.id == thisId;
            });
            editProject[0].name = project.name;
            editProject[0].startDate = project.startDate;
            editProject[0].endDate = project.endDate;
            editProject[0].status = project.status;
        }
        this.projectForm.reset();
        this.projectForm.patchValue(this.project);
    }

    newProject():void {
        this.projectForm.reset();
        this.projectForm.patchValue(this.project);
    }

    editProject(project):void {
        let startDate = new Date(project.startDate);
        let endDate = new Date(project.endDate);
        let editProject = {
            id: project.id,
            name: project.name,
            startDate: this._datePipe.transform(startDate, 'yyyy-MM-dd HH:mm'),
            endDate: this._datePipe.transform(endDate, 'yyyy-MM-dd HH:mm'),
            status: project.status
        };

        this.projectForm.patchValue(editProject);
    }

    checkStartDate(value):void {
        let d = new Date(value);

        if(d.getTime()) {
            this.startDateError = false;
        } else {
            this.startDateError = true;
        }
    }

    checkEndDate(value):void {
        let d = new Date(value);
        let nd = new Date();

        if(d.getTime()) {
            if(d.getTime() < nd.getTime()) {
                this.projectForm.controls['status'].setValue("ACTIVE");
            } else {
                this.projectForm.controls['status'].setValue("NOT_ACTIVE");
            }
            this.endDateError = false;
        } else {
            this.endDateError = true;
        }
    }

}

import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {RouterModule} from "@angular/router";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ProjectComponent} from './project.component';
import {ProjectService} from "./project.service";
import {KeyModule} from "../key/key.module";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            { path: 'projects', component: ProjectComponent }
        ]),
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        KeyModule
    ],
    declarations: [
        ProjectComponent
    ],
    providers: [
        ProjectService,
        DatePipe
    ]
})
export class ProjectModule { }

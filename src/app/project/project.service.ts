import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {IProject} from "./project";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ProjectService {

    constructor(private _http:HttpClient) {}

    //getProjects(): Observable<IProject[]> {
    getProjects(): Observable<any> {
        return this._http.get('/assets/api/projects.json', httpOptions);
    }
}

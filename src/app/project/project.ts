export interface IProject {
    id: number,
    name: string,
    startDate: string,
    endDate: string,
    status: string
}
